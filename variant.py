'''
Code generation for a variant (aka "tagged union") for C++.
'''
import re

class Variant:
    def __init__(self, typename: str):
        self.typename = typename
        self.fields: dict[str, str] = {}
        self.no_copy: set[str] = set()
        self.no_move: set[str] = set()

    def gen(self) -> str:
        '''Generate C++ code string for variant'''
        assert type_uniqueness(self), 'Types must be unique.'
        code = generate_cpp(self)
        return code

    def gen_file(self, filepath: str) -> int:
        '''Generate C++ code string for variant and write it to file'''
        code = generate_cpp(self)
        assert type_uniqueness(self), 'Types must be unique.'
        with open(filepath, 'w') as f:
            return f.write(code)

def tag_enum(v: Variant) -> str:
    enum  = f'''
    enum class Kind : int {{
        undefined = 0,
    {'\n'.join([f"    {name}," for name in v.fields.keys()])}
    }};
    '''
    return enum

def union(v: Variant) -> str:
    union  = f'''
        union Payload {{
        {'\n'.join([f"    {t} _{name};" for name, t in v.fields.items()])}

        Payload(){{}}
        ~Payload(){{}}
        }};
        Payload payload;
        Kind tag;
    '''
    return union

def getters(v: Variant) -> list[str]:
    getters = [f'''
    {t}& {name}(){{
        assert(this->tag == Kind::{name});
        return payload._{name};
    }}
    ''' for name, t in v.fields.items()]

    getters.append(f'''
    auto variant() const {{
        return tag;
    }}
    ''')

    return getters

# Execute a snippet of code if variant matches target type
def exec_if(target: str, fn: str, else_fn: str) -> str:
    src = f'''
    if(this->tag == Kind::{target}){{
        {fn};
    }}
    else {{
        {else_fn};
    }}
    '''
    return src

def constructors(v: Variant, move_fn = 'std::move') -> list[str]:
    ctors = [f'''
    {v.typename}()
        : tag{{Kind::undefined}}{{}}
    ''']

    items = filter(lambda kv: kv[0] not in v.no_copy, v.fields.items())

    copy_ctors = [f'''
    explicit
    {v.typename}({t} const& val)
        : tag{{Kind::{name}}}
    {{
        new (&payload._{name}) {t}(val);
    }}
    ''' for name, t in items ]

    items = filter(lambda kv: kv[0] not in v.no_move, v.fields.items())
    move_ctors = [f'''
    explicit
    {v.typename}({t} && val)
        : tag{{Kind::{name}}}
    {{
        new (&payload._{name}) {t}({move_fn}(val));
    }}
    ''' for name, t in items ]

    return ctors + copy_ctors + move_ctors

def assigns(v: Variant, move_fn = 'std::move') -> list[str]:
    items = filter(lambda kv: kv[0] not in v.no_copy, v.fields.items())

    copy_assigns = [f'''
    void operator=({t} const& val){{
        {exec_if(name, f'payload._{name} = val', f'destroy_active(); new (&payload._{name}) {t}(val)')}

        this->tag = Kind::{name};
    }}
    ''' for name, t in items]


    items = filter(lambda kv: kv[0] not in v.no_move, v.fields.items())
    move_assigns = [f'''
    void operator=({t} && val){{
        {exec_if(name, f'payload._{name} = {move_fn}(val)', f'destroy_active(); new (&payload._{name}) {t}({move_fn}(val))')}

        this->tag = Kind::{name};
    }}
    ''' for name, t in items]

    return copy_assigns + move_assigns

def destructor(v: Variant) -> str:
    dtor = f'''
    void destroy_active(){{
        switch(tag){{
        {'\n'.join([f"case Kind::{name}: destroy(payload._{name}); break;" for name in v.fields.keys()])}
        case Kind::undefined: break;
        }}
        this->tag  = Kind::undefined;
    }}

    ~{v.typename}(){{
        destroy_active();
    }}
    '''
    return dtor

def type_uniqueness(v: Variant) -> bool:
    vals = set(v.fields.values())
    return len(vals) == len(v.fields)

def generate_cpp(v: Variant) -> str:
    # Create raw output
    out = f'''struct {v.typename}{{
        { tag_enum(v) +
          union(v) +
          '\n'.join(getters(v) + constructors(v) + assigns(v)) +
          destructor(v) }
        }};
    '''

    pattern = re.compile(r'\s+', flags=re.MULTILINE)
    out = pattern.sub(' ', out)

    out = (out.replace(';', ';\n')
              .replace(', ', ',\n')
              .replace('} ', '}\n')
              .replace('{ ', '{\n'))

    out = '\n'.join(map(lambda x: x.strip(), out.split('\n')))

    # Make it (slightly) more readable
    start, current = 0, 0
    indented = []
    level = 0
    prev_c = ''
    for c in out[:len(out) - 1]:
        current += 1
        if c == '}':
            level -= 1
        if c == '\n':
            n = level - (1 if prev_c == '{' else 0)
            indented.append('    ' * n + out[start:current])
            start = current
        if c == '{':
            level += 1
        prev_c = c
    indented.append(out[start:current]) # Last line

    out = ''.join(indented)
    return out

