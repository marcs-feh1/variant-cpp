from variant import Variant

FILE = 'var.hpp'

var = Variant('Variant')
var.fields = {
    'ptr': 'std::unique_ptr<int>',
    'list': 'std::vector<int>',
    'index': 'int',
}
var.no_copy = {'ptr'}

print(f'Wrote {var.gen_file(FILE)}B to {FILE}')

