#include <iostream>
#include <memory>
#include <vector>
#include <new>

#define assert(x) if(!(x)){ throw "Assertion failure"; }

template<typename T>
void destroy(T& x){
	x.~T();
}

#include "var.hpp"

int main(){
	auto v = Variant(std::vector<int>(200));
	v.list().push_back(100);
	std::cout << int(v.variant()) << '\n';
	v = std::vector<int>();
	v = 100;
	v = v.index() + 10;
	std::cout << int(v.variant()) << '\n';
	v = std::vector<int>(200);
	v = std::make_unique<int>();
	*v.ptr() = 6969;
	std::cout << *v.ptr() << '\n';
}
