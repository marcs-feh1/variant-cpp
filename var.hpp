struct Variant{
    enum class Kind : int {
        undefined = 0,
        ptr,
        list,
        index,
    };
    union Payload {
        std::unique_ptr<int> _ptr;
        std::vector<int> _list;
        int _index;
        Payload(){}
        ~Payload(){}
    };
    Payload payload;
    Kind tag;
    std::unique_ptr<int>& ptr(){
        assert(this->tag == Kind::ptr);
        return payload._ptr;
    }
    std::vector<int>& list(){
        assert(this->tag == Kind::list);
        return payload._list;
    }
    int& index(){
        assert(this->tag == Kind::index);
        return payload._index;
    }
    auto variant() const {
        return tag;
    }
    Variant() : tag{Kind::undefined}{}
    explicit Variant(std::vector<int> const& val) : tag{Kind::list}
    {
        new (&payload._list) std::vector<int>(val);
    }
    explicit Variant(int const& val) : tag{Kind::index}
    {
        new (&payload._index) int(val);
    }
    explicit Variant(std::unique_ptr<int> && val) : tag{Kind::ptr}
    {
        new (&payload._ptr) std::unique_ptr<int>(std::move(val));
    }
    explicit Variant(std::vector<int> && val) : tag{Kind::list}
    {
        new (&payload._list) std::vector<int>(std::move(val));
    }
    explicit Variant(int && val) : tag{Kind::index}
    {
        new (&payload._index) int(std::move(val));
    }
    void operator=(std::vector<int> const& val){
        if(this->tag == Kind::list){
            payload._list = val;
        }
        else {
            destroy_active();
            new (&payload._list) std::vector<int>(val);
        }
        this->tag = Kind::list;
    }
    void operator=(int const& val){
        if(this->tag == Kind::index){
            payload._index = val;
        }
        else {
            destroy_active();
            new (&payload._index) int(val);
        }
        this->tag = Kind::index;
    }
    void operator=(std::unique_ptr<int> && val){
        if(this->tag == Kind::ptr){
            payload._ptr = std::move(val);
        }
        else {
            destroy_active();
            new (&payload._ptr) std::unique_ptr<int>(std::move(val));
        }
        this->tag = Kind::ptr;
    }
    void operator=(std::vector<int> && val){
        if(this->tag == Kind::list){
            payload._list = std::move(val);
        }
        else {
            destroy_active();
            new (&payload._list) std::vector<int>(std::move(val));
        }
        this->tag = Kind::list;
    }
    void operator=(int && val){
        if(this->tag == Kind::index){
            payload._index = std::move(val);
        }
        else {
            destroy_active();
            new (&payload._index) int(std::move(val));
        }
        this->tag = Kind::index;
    }
    void destroy_active(){
        switch(tag){
            case Kind::ptr: destroy(payload._ptr);
            break;
            case Kind::list: destroy(payload._list);
            break;
            case Kind::index: destroy(payload._index);
            break;
            case Kind::undefined: break;
        }
        this->tag = Kind::undefined;
    }
    ~Variant(){
        destroy_active();
    }
};